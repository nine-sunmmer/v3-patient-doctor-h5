import { createPinia } from 'pinia'
import persist from 'pinia-plugin-persist'

const pinia = createPinia().use(persist)

export default pinia

// 导入 并导出 user 模块
export * from '@/store/modules/user'
export * from '@/store/modules/consult'
