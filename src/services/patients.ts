import type { Patient } from '@/types/data'
import request from '@/utils/request'

/**
 * 获取患者列表信息
 */
export const getPatientList = () => {
  return request<Patient[]>('/patient/mylist', 'GET')
}

/**
 * 添加患者信息
 * @param data
 */
export const addPatient = (data: Patient) => {
  return request<Patient[]>('/patient/add', 'post', data)
}

/**
 * 编辑患者
 * @param data
 */
export const editPatient = (data: Patient) => {
  return request<Patient[]>('/patient/update', 'put', data)
}

/**
 * 删除患者
 * @param id
 */
export const delPatient = (id: string) => {
  return request<Patient[]>('/patient/del/' + id, 'DELETE')
}

/**
 * 获取患者详情
 * @param id
 */
export const getDetailPatient = (id: string) =>
  request<Patient>('/patient/info/' + id, 'GET')
