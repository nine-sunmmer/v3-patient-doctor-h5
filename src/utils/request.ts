import axios, { type Method } from 'axios'
import type { IResult } from '@/types/data'
import { useUserStore } from '@/store'
import { showToast } from 'vant'
import router from '@/router'
// 1. 创建一个axios 实例
/* 
  1.1 配置基地址, 请求时间响应
 */

const baseURL = 'https://consult-api.itheima.net'
const instance = axios.create({
  timeout: 10000,
  baseURL
})

// 1.2 配置请求响应拦截器
instance.interceptors.request.use(
  (config) => {
    // 1.3 携带token
    const userStore = useUserStore()
    const token = userStore.userInfo.token
    if (token && config.headers) {
      config.headers['Authorization'] = `Bearer ${token}`
    }
    return config
  },
  (err) => Promise.reject(err)
)

// 1.3 响应拦截器
instance.interceptors.response.use(
  (res) => {
    // 1.4 请求 结果是响应式成功的但 结果不是我们想要的
    if (res.data.code !== 10000) {
      showToast(res.data.message || '未知错误')
      return Promise.reject(res)
    }
    // 1.5 成功后解耦一下 响应信息
    return res.data
  },
  (err) => {
    // 1.6 如果是 401 得到错误
    if (err.response.status === 401) {
      // 清除 用户信息. 跳转到登录页
      const userStore = useUserStore()
      userStore.delUserInfo()
      console.log(router.currentRoute, '1111')

      router.push({
        path: '/login',
        query: {
          returnURL: router.currentRoute.value.fullPath
        }
      })
    }
    return Promise.reject(err)
  }
)

// 2. 封装一个工具函数 用于发送请求
// 2.1 设置传参 method url submitData
// 2.2 根据 传递泛型, 决定 返回的数据类型
const request = <T>(url: string, method: Method = 'GET', submitData?: any) => {
  return instance.request<T, IResult<T>>({
    method,
    url,
    [method.toLocaleUpperCase() === 'GET' ? 'params' : 'data']: submitData
  })
}

export { baseURL, instance }
export default request
