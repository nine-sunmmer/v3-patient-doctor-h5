import { ConsultType, IllnessTime, OrderType } from '@/enums'
import type { Patient } from './data'

// 图片列表
export type Image = {
  /** 图片ID */
  id: string
  /** 图片地址 */
  url: string
}

// 问诊记录
export type Consult = {
  /** 问诊记录ID */
  id: string
  /** 问诊类型 */
  type: ConsultType
  /** 快速问诊类型，0 普通 1 三甲 */
  illnessType: 0 | 1
  /** 科室ID */
  depId: string
  /** 疾病描述 */
  illnessDesc: string
  /** 疾病持续时间 */
  illnessTime: IllnessTime | ''
  /** 是否就诊过，0 未就诊过  1 就诊过 */
  consultFlag: 0 | 1 | ''
  /** 图片数组 */
  pictures: Image[]
  /** 患者ID */
  patientId: string
  /** 优惠券ID */
  couponId: string
}

// 问诊记录-全部可选
export type PartialConsult = Partial<Consult>
// Required 转换为全部必须   Partial 转换问全部可选  两个内置的泛型类型
// export type RequiredConsult = Required<PartialConsult>

export type IllnessDescType = Pick<
  PartialConsult,
  'illnessDesc' | 'pictures' | 'consultFlag' | 'illnessTime'
>

/**
 * 科室类型
 */
export interface DepItem {
  id: string
  name: string
  child: DepItemChild[]
}

/**
 * 科室子类型
 */
interface DepItemChild {
  id: string
  name: string
  avatar: string
}

// 问诊订单预支付传参
export type ConsultOrderPreParams = Pick<PartialConsult, 'type' | 'illnessType'>

// 问诊订单预支付信息
export type ConsultOrderPreData = {
  /** 积分抵扣 */
  pointDeduction: number
  /** 优惠券抵扣 */
  couponDeduction: number
  /** 优惠券ID */
  couponId: string
  /** 需付款 */
  payment: number
  /** 实付款 */
  actualPayment: number
}

/**
 * 支付订单参数
 */
export type PayParams = {
  paymentMethod: 0 | 1
  orderId: string
  payCallback: string
}

/**
 * 咨询室内 订单信息
 */
export interface ConsultOrderInfo {
  id: string
  orderNo: string
  type: string
  createTime: string
  patientInfo: Patient
  illnessDesc: string
  recordId: string
  status: OrderType
  statusValue: string
  cancelProcess?: any
  countdown: number
  payment: number
  pointDeduction: number
  couponDeduction: number
  actualPayment: number
  creator: string
  evaluateFlag: number
  pictures: any[]
  illnessTime: number
  consultFlag: 0 | 1
  docInfo: DocInfo
}

/***
 * 医生信息
 */
interface DocInfo {
  id: string
  name: string
  avatar: string
  depName: string
  positionalTitles: string
  hospitalName: string
  gradeName: string
  evaluateId?: string
}

/**
 * 问诊订单类型 列表
 */
export interface OrderItemList {
  total: number
  pageTotal: number
  rows: Row[]
}

/**
 * 问诊订单单个列表类型
 */
export interface Row {
  id: string
  orderNo: string
  type: string
  typeValue: string
  createTime: string
  patientInfo: PatientInfo
  illnessDesc: string
  docInfo: DocInfoI
  status: number
  statusValue: string
  cancelReason?: string
  cancelReasonValue?: string
  cancelProcess?: string
  payment: number
  evaluateId?: string
  illnessTime: number
  consultFlag: number
  prescriptionId?: string
}

interface DocInfoI {
  name: string
  id?: string
  avatar?: string
  depName?: string
  positionalTitles?: string
  hospitalName?: string
  gradeName?: string
}

interface PatientInfo {
  name: string
  idCard: string
  gender: number
  genderValue: string
  age: number
  id: string
}
