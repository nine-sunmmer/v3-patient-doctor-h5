import type { ByPassword, loginType, User } from '@/types/data'
import request from '@/utils/request'

/**
 * 通过密码登录
 * @param data
 */
export const loginByPassword = (data: { mobile: string; password: string }) => {
  return request<ByPassword>('/login/password', 'POST', data)
}

/**
 * 获取验证码
 * @param data
 */
export const getCode = (data: { mobile: string; type: loginType }) => {
  return request<{ code: string }>('/code', 'GET', data)
}

/**
 * code 登录
 * @param data
 */
export const loginByCode = (data: { mobile: string; code: string }) => {
  return request<ByPassword>('/login', 'POST', data)
}

/**
 * 网站有记录时 qq 登录
 * @param openId
 */
export const loginByQQ = (openId: string) =>
  request<User>('/login/thirdparty', 'POST', { openId, source: 'qq' })

/**
 * 新用户qq绑定时 登录
 * @param data
 */
export const bingQQLogin = (data: {
  mobile: string
  openId: string
  code: string
}) => request<User>('/login/binding', 'POST', data)
