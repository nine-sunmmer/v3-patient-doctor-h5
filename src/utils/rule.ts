import type { FieldRule, FieldRuleValidator } from 'vant'

const MobileRules: FieldRule[] = [
  {
    message: '请输入手机号',
    required: true
  },
  {
    pattern: /^1[3-9]\d{9}$/,
    message: '请输入正确格式的手机号'
  }
]

const PasswordRules: FieldRule[] = [
  {
    message: '请输入密码',
    required: true
  },
  {
    pattern: /^[A-Za-z0-9]{8,28}$/,
    message: '请输入8-28位字符密码'
  }
]

const CodeRules: FieldRule[] = [
  {
    message: '请输入验证码',
    required: true
  },
  {
    pattern: /^\d{6}$/,
    message: '验证码为6位数字'
  }
]

const RequiredRule: FieldRule[] = [
  {
    message: '必填',
    required: true
  }
]
const idCardValide: FieldRuleValidator = (val) => {
  const reg =
    /^[1-9]\d{5}(?:18|19|20)\d{2}(?:0[1-9]|10|11|12)(?:0[1-9]|[1-2]\d|30|31)\d{3}[\dXx]$/
  if (!reg.test(val)) return false
  return true
}

const idCardRule: FieldRule = {
  validator: idCardValide,
  message: '身份证格式不对'
}

export { PasswordRules, MobileRules, CodeRules, RequiredRule, idCardRule }
