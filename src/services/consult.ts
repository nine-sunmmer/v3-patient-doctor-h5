import type { ConsultType } from '@/enums'
import type {
  ConsultOrderInfo,
  ConsultOrderPreData,
  ConsultOrderPreParams,
  DepItem,
  OrderItemList,
  PartialConsult,
  PayParams
} from '@/types/consult'
import type {
  DoctorPage,
  KnowledgePage,
  KnowledgeParams,
  LikeType,
  PageParams
} from '@/types/data'
import type { EvaluateDoc } from '@/types/romm'
import request from '@/utils/request'

/**
 * 获取 首页文章列表
 * @param params
 */
export const getKonowLedgeList = (params: KnowledgeParams) => {
  return request<KnowledgePage>('/patient/home/knowledge', 'GET', params)
}

/**
 * 获取推荐关注的医生列表
 * @param params
 */
export const getLikeDoctor = (params: PageParams) => {
  return request<DoctorPage>('/home/page/doc', 'GET', params)
}

/**
 * 关注
 * @param data
 */
export const likeTarget = (data: { type: LikeType; id: string }) =>
  request('/like', 'POST', data)

/**
 * 获取科室
 */
export const getAllDep = () => request<DepItem[]>('/dep/all')

/**
 * 文件 患者图片上传
 * @param file
 */
export const uploadFile = (file: File) => {
  const fd = new FormData()
  fd.append('file', file)
  return request<{ id: string; url: string }>('/upload', 'POST', fd)
}

/**
 * 获取患者预支付信息
 */
export const getConsultOrder = (params: ConsultOrderPreParams) => {
  return request<ConsultOrderPreData>(
    '/patient/consult/order/pre',
    'GET',
    params
  )
}

/**
 * 生成订单
 */
export const createPayOrder = (data: PartialConsult) =>
  request<{ id: string }>('/patient/consult/order', 'POST', data)

/**
 * 支付订单
 */
export const payOrder = (data: PayParams) =>
  request<{ payUrl: string }>('/patient/consult/pay', 'POST', data)

/**
 * 查询订单状态
 * @param params
 */
export const checkOrder = (params: { orderId: string }) =>
  request<ConsultOrderInfo>('/patient/consult/order/detail', 'GET', params)

/**
 * 评价医生
 * @param data
 */
export const evaluateDocFn = (data: EvaluateDoc) =>
  request('/patient/order/evaluate', 'POST', data)

/**
 * 获取问诊订单列表
 */
export const getConsultOrderList = (params: {
  current: number
  pageSize: number
  type: ConsultType
}) => request<OrderItemList>('/patient/consult/order/list', 'GET', params)

/**
 * 取消订单
 */
export const cancelConsultOrder = (id: string) =>
  request('/patient/order/cancel/' + id, 'PUT')

/**
 * 删除订单
 */
export const delOrder = (id: string) =>
  request('/patient/order/' + id, 'DELETE')

/**
 * 获取订单详情
 */
export const getOrderDeatil = (id: string) =>
  request<ConsultOrderInfo>('/patient/consult/order/detail', 'GET', {
    orderId: id
  })
