import { createApp } from 'vue'
import pinia from './store'
import App from './App.vue'
import router from './router'
// 2. 引入组件样式
import 'vant/lib/index.css'
import '@/styles/main.scss'
import 'virtual:svg-icons-register'

const app = createApp(App)

app.use(pinia)
app.use(router)

app.mount('#app')
