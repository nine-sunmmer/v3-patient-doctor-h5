import type {
  AddressItem,
  Logistics,
  OrderDetail,
  OrderPre
} from '@/types/order'
import request from '@/utils/request'

/**
 * 展示电子处方图
 */
export const getShowPerImg = (id: string) =>
  request<{ url: string; id: string }>('/patient/consult/prescription/' + id)

/**
 * 获取药品订单 预支付信息
 */
export const getMedicinePayInfo = (id: string) =>
  request<OrderPre>('/patient/medicine/order/pre', 'GET', {
    prescriptionId: id
  })

/**
 * 获取收货地址列表
 */
export const getAddress = () =>
  request<AddressItem[]>('/patient/order/address', 'GET')

/**
 * 生成药品订单id
 */
export const createDrugOrderid = (data: {
  id: string
  couponId?: string
  addressId: string
}) => request<{ id: string }>('/patient/medicine/order', 'post', data)

/**
 获取药品 订单详情
 *  */
export const getMedicineOrderInfo = (id: string) =>
  request<OrderDetail>('/patient/medicine/order/detail/' + id)

/**
 * 查询物流信息
 */
export const getOrderLogistics = (id: string) =>
  request<Logistics>(`/patient/order/${id}/logistics/`)
