import type { User } from '@/types/data'
import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useUserStore = defineStore(
  'user',
  () => {
    // 1. 用户信息
    const userInfo = ref<User>({ id: 19 } as unknown as User)
    const returnUrl = ref('')

    // 2. 更新用户信息
    const updateUserInfo = (user: User) => {
      userInfo.value = user
    }

    // 3. 删除用户信息
    const delUserInfo = () => {
      userInfo.value = {} as User
    }

    // 4. qq 登录时 回条地址
    const saveReturnUrl = (url: string) => {
      returnUrl.value = url
    }
    return { userInfo, updateUserInfo, delUserInfo, saveReturnUrl, returnUrl }
  },
  {
    persist: {
      enabled: true,
      strategies: [
        {
          storage: localStorage
        }
      ]
    }
  }
)
