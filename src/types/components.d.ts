import CpNavBar from '@/components/cp-nav-bar.vue'
import CpSvgIcons from '@/components/cp-svg-icons.vue'
import CpRadioBtn from '@/components/cp-radio-btn.vue'
import CpMore from '@/components/cp-more.vue'
import CpConsultPay from '@/components/cp-consult-pay.vue'
declare module 'vue' {
  interface GlobalComponents {
    CpNavBar: typeof CpNavBar
    CpSvgIcons: typeof CpSvgIcons
    CpRadioBtn: typeof CpRadioBtn
    CpMore: typeof CpMore
    CpConsultPay: typeof CpConsultPay
  }
}
