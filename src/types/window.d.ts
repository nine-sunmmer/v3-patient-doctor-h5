interface Window {
  _AMapSecurityConfig: {
    securityJsCode: string
  }
  QC: {
    Login: {
      check: () => boolean
      getMe: (fn: (openId: string) => void) => void
    }
  }
}
