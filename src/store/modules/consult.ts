import type { ConsultType } from '@/enums'
import type { Consult, PartialConsult } from '@/types/consult'
import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useConsultStore = defineStore(
  'cp-consult',
  () => {
    // 1. 定义问诊数据类型
    const constSultData = ref<PartialConsult>({})

    // 2. 设置问诊类型
    const setConsultType = (type: ConsultType) => {
      constSultData.value.type = type
    }

    // 3. 设置快速问诊类型
    const setIllnessType = (type: 0 | 1) => {
      constSultData.value.illnessType = type
    }

    // 4. 设置选择科室
    const setDeparment = (id: string) => {
      constSultData.value.depId = id
    }

    // 5. 设置图文问诊 病情描述
    const setIllnessDescrible = (
      data: Pick<
        Consult,
        'illnessDesc' | 'consultFlag' | 'illnessTime' | 'pictures'
      >
    ) => {
      constSultData.value.consultFlag = data.consultFlag
      constSultData.value.illnessTime = data.illnessTime
      constSultData.value.illnessDesc = data.illnessDesc
      constSultData.value.pictures = data.pictures
    }

    // 6. 选择患者
    const setPatient = (id: string) => {
      constSultData.value.patientId = id
    }

    // 7. 选择优惠卷
    const setCouPon = (id: string) => {
      constSultData.value.couponId = id
    }

    // 8. 清空就诊信息
    const clearConsultInfo = () => {
      constSultData.value = {}
    }

    return {
      constSultData,
      setConsultType,
      setIllnessType,
      setPatient,
      setCouPon,
      setIllnessDescrible,
      setDeparment,
      clearConsultInfo
    }
  },
  {
    persist: {
      enabled: true,
      strategies: [
        {
          storage: localStorage
        }
      ]
    }
  }
)
