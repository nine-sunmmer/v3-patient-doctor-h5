import type { IllnessTime } from '@/enums'
import { flagOptions, illnessTimeOptions } from '@/services/constants'

// 获取患病时间
export const getIllnessTime = (val: IllnessTime) => {
  return illnessTimeOptions.find((item) => item.value === val)?.label || '未知'
}

// 获取 是否就诊
export const getConsultFlag = (flag: 1 | 0) => {
  return flagOptions.find((item) => item.value === flag)?.label || '未知'
}
