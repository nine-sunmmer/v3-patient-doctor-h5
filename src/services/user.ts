import type { OmitUser } from '@/types/data'
import request from '@/utils/request'

/**
 * 个人中心 获取用户信息
 */
export const getUserInfo = () => {
  return request<OmitUser>('/patient/myUser')
}
