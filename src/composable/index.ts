import { OrderType } from '@/enums'
import { cancelConsultOrder, delOrder, likeTarget } from '@/services/consult'
import { getMedicineOrderInfo, getShowPerImg } from '@/services/durgOrder'
import { getCode } from '@/services/login'
import type { Row } from '@/types/consult'
import type { LikeType, loginType } from '@/types/data'
import type { OrderDetail } from '@/types/order'
import type { FormInstance } from 'vant'
import { showImagePreview } from 'vant/lib/image-preview'
import {
  showFailToast,
  showLoadingToast,
  showSuccessToast
} from 'vant/lib/toast'
import { onBeforeUnmount, ref } from 'vue'

/**
 * 关注逻辑 复用
 * @param type
 */
export const useFllow = (type: LikeType) => {
  // loading 放在外面, 相当于 所有组件 公用 一个 loading(会有问题)
  const loading = ref(false)
  const clickLike = async (item: { likeFlag: 1 | 0; id: string }) => {
    try {
      loading.value = true
      await likeTarget({ type, id: item.id })
      item.likeFlag = item.likeFlag ? 0 : 1
    } finally {
      loading.value = false
    }
  }
  return { loading, clickLike }
}

/**
  展示处方电子图
 *  
 * */
export const useShowPerImg = () => {
  const showPerscriptionImg = async (id: string) => {
    const loading = showLoadingToast('加载中')
    try {
      const res = await getShowPerImg(id)
      showImagePreview([res.data.url])
    } finally {
      loading.close()
    }
  }
  return { showPerscriptionImg }
}

/**
 *  取消订单操作
 *
 * */
export const useCancelOrder = () => {
  const loading = ref(false)
  const cancelOrder = async (item: Row) => {
    try {
      loading.value = true
      await cancelConsultOrder(item.id)
      showSuccessToast('取消成功')
      item.status = OrderType.ConsultCancel
      item.statusValue = '已取消'
    } catch {
      showFailToast('取消失败')
    }
    loading.value = false
  }

  return { cancelOrder, loading }
}

/**
 * 删除订单操作
 * 由于删除订单后的操作 需要做不同的事情, (例: 订单列表中删除完成后, 需要修改父组件传递的值, 这个订单删除, 而在订单详情中, 删除后需要 跳转到 订单页面)
 */
export const useDelOrder = (callback: (id: string) => void) => {
  const delLoading = ref(false)
  const delConsultOrder = async (id: string) => {
    try {
      delLoading.value = true
      await delOrder(id)
      showSuccessToast('删除成功')

      // 根据回调函数 做不同的事情
      callback(id)
    } catch (error) {
      showFailToast('删除失败')
    }
    delLoading.value = false
  }
  return { delLoading, delConsultOrder }
}

/**
 * 获取药品订单详情
 */

export const useGetOrderMedicalInfo = (id: string) => {
  const drugInfo = ref<OrderDetail>()

  const getOrderPayInfo = async () => {
    const res = await getMedicineOrderInfo(id)
    drugInfo.value = res.data
  }

  return { drugInfo, getOrderPayInfo }
}

/**
 * 倒计时 和 发送验证码效果
 * @param loginType
 */
export const useCutDown = (loginType: loginType) => {
  const time = ref(0)
  let timer: number = -1
  const mobile = ref('13230000005')
  const code = ref('')
  const refForm = ref<FormInstance | null>(null)

  const useInterval = () => {
    if (!time.value) return
    timer = setInterval(() => {
      time.value--
      if (time.value <= 0) clearInterval(timer as number)
    }, 1000)
  }
  // 发送验证码功能
  // 1.1 校验手机号是否填入
  // 1.2 发送请求, 获取code
  // 1.3 成功后, 倒计时效果
  const senCode = async () => {
    // 校验成功后
    await refForm.value?.validate('mobile')
    if (time.value) return
    const res = await getCode({ mobile: mobile.value, type: loginType })
    code.value = res.data.code
    time.value = 60
    useInterval()
  }

  onBeforeUnmount(() => {
    clearInterval(timer)
  })

  return { senCode, time, code, refForm, mobile }
}
