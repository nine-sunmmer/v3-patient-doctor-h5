import { useUserStore } from '@/store'
import { createRouter, createWebHistory } from 'vue-router'
import NProgress from 'nprogress'
NProgress.configure({
  showSpinner: false
})
import 'nprogress/nprogress.css'
NProgress
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/login',
      component: () => import('@/views/Login/index.vue'),
      meta: {
        name: '首页'
      }
    },
    {
      path: '/',
      redirect: '/home',
      component: () => import('@/views/layout/index.vue'),
      children: [
        {
          path: '/home',
          component: () => import('@/views/home/index.vue'),
          meta: {
            name: '首页',
            icon: 'index'
          }
        },
        {
          path: '/article',
          component: () => import('@/views/article/index.vue'),
          meta: {
            name: '健康百科',
            icon: 'article'
          }
        },
        {
          path: '/notify',
          component: () => import('@/views/notify/index.vue'),
          meta: {
            name: '消息中心',
            icon: 'notice'
          }
        },
        {
          path: '/user',
          component: () => import('@/views/user/index.vue'),
          meta: {
            name: '我的',
            icon: 'mine'
          }
        }
      ]
    },
    {
      path: '/user/patient',
      meta: {
        name: '家庭档案'
      },
      component: () => import('@/views/user/PatientVue.vue')
    },
    {
      path: '/consult/fast',
      meta: {
        name: '极速问诊'
      },
      component: () => import('@/views/consult/consoutFast.vue')
    },
    {
      path: '/consult/dep',
      meta: {
        name: '选择科室'
      },
      component: () => import('@/views/consult/ConsultDep.vue')
    },
    {
      path: '/consult/illness',
      meta: {
        name: '病情描述'
      },
      component: () => import('@/views/consult/IllnessDecs.vue')
    },
    {
      path: '/consult/pay',
      meta: {
        name: '问诊支付'
      },
      component: () => import('@/views/consult/ConsultPay.vue')
    },
    {
      path: '/room',
      meta: {
        name: '问诊室'
      },
      component: () => import('@/views/Room/index.vue'),
      // 进入路由时触发
      beforeEnter(to) {
        if (to.query.payResult === 'false' || !to.query.orderId) {
          return '/user/consult'
        }
      }
    },
    {
      path: '/user/consult',
      meta: {
        name: '问诊记录'
      },
      component: () => import('@/views/user/ConsultPage.vue')
    },
    {
      path: '/user/consult/:id',
      meta: {
        name: '问诊详情'
      },
      component: () => import('@/views/user/ConsultDeatil.vue')
    },
    {
      path: '/order/pay',
      meta: {
        name: '药品支付'
      },
      component: () => import('@/views/order/OrderPay.vue')
    },
    {
      path: '/order/pay/result',
      meta: {
        name: '药品支付结果'
      },
      component: () => import('@/views/order/OrderPayResult.vue')
    },
    {
      path: '/order/:id',
      meta: {
        name: '药品订单详情'
      },
      component: () => import('@/views/order/OrderDeatil.vue')
    },
    {
      path: '/order/logistics/:id',
      meta: {
        name: '物流详情'
      },
      component: () => import('@/views/order/OrderLogistics.vue')
    },
    {
      path: '/login/callback',
      component: () => import('@/views/Login/LoginCallback.vue'),
      meta: { name: 'QQ登录-绑定手机' }
    }
  ]
})

// vue3 的路由 不需要 next (兼容 vue2 可以写)
// 1.1 函数 return true, 或不 return (放行)
// 1.2 如果想拦截 直接写 return '/xx'(路径)

// 1.1 判断是否拥有 token, 如果没有就 拦截
// 1.2 设置白名单 如果没有token 也可访问

const whiltList = ['/login', '/login/callback']
router.beforeEach((to) => {
  NProgress.start()
  const token = useUserStore().userInfo.token
  // 如果没有 token 且不在白名单范围
  if (!token && !whiltList.includes(to.path))
    return `/login?returnURL=${to.fullPath}`
})

router.afterEach((to) => {
  document.title = to.meta.name || '未知'
  NProgress.done()
})

export default router
